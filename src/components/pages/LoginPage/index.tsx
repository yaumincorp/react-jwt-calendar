import { SetStateAction, useState } from "react";
import { useNavigate } from "react-router-dom";

const LoginPage = () => {
  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState("");
  const [emailError, setEmailError] = useState("");

  //Put it in helpers.js
  function isValidEmail(email: string) {
    return /\S+@\S+\.\S+/.test(email);
  }

  let navigate = useNavigate();

  const handleChangeUserName = (e: { target: { value: string } }) => {
    setUserName(e.target.value);
  };

  const handleChangeEmail = (e: { target: { value: string } }) => {
    if (!isValidEmail(e.target.value)) {
      setEmailError("Email is invalid");
    } else {
      setEmailError("");
    }

    setEmail(e.target.value);
  };

  const handleChangePassword = (e: { target: { value: string } }) => {
    setPassword(e.target.value);
  };

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    //reqres registered sample user
    const loginData = {
      //username: userName,
      email: email,
      password: password,
    };

    const response = await fetch("https://reqres.in/api/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(loginData),
    });
    if (!response.ok) {
      console.log("response", response);
      throw new Error("Bad login");
    } else {
      //get token from response
      const data = await response.json();
      console.log("data", data);
      //set JWT token to local
      //localStorage.setItem("token", token);

      //set token to axios common header
      //setAuthToken(token);
    }

    let token = {};

    setUserName("");
    setPassword("");

    navigate("/");
  };

  return (
    <div className="md:container md:mx-auto">
      <h1 className="text-3xl font-bold">Login</h1>
      <div className="flex flex-row my-4 justify-center">
        <form className="flex flex-col items-center" onSubmit={onSubmit}>
          {/* <input
            className="my-4 p-1 border-2 border-slate-200"
            type="text"
            onChange={(e) => handleChangeUserName(e)}
            placeholder="Username"
            value={userName}
          /> */}
          <input
            className="my-4 p-1 border-2 border-slate-200"
            type="email"
            onChange={(e) => handleChangeEmail(e)}
            placeholder="Email"
            value={email}
          />
          <input
            className="my-4 p-1 border-2 border-slate-200"
            type="password"
            onChange={(e) => handleChangePassword(e)}
            placeholder="Password"
            value={password}
          />
          <button
            className="bg-slate-600 hover:bg-slate-800 text-white rounded-full px-4"
            type="submit"
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
};

export default LoginPage;
