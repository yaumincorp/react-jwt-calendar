import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { Link, Route, Routes } from "react-router-dom";
import LoginPage from "./components/pages/LoginPage";
import ProfilePage from "./components/pages/ProfilePage";

function App() {
  return (
    <div className="App">
      <header className="">
        <nav
          className="nav px-2 bg-gray-800 text-white flex items-center justify-between sm:h-10 lg:justify-start"
          aria-label="Global"
        >
          <ul className="flex">
            <li className="flex-auto px-3">
              <Link to="/">Profile</Link>
            </li>
            <li className="flex-auto px-3">
              <Link to="/login">Login</Link>
            </li>
            <li className="flex-auto px-3">
              <Link to="/calendar">Calendar</Link>
            </li>
          </ul>
        </nav>
      </header>
      <div>
        <hr />

        {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}
        <Routes>
          <Route path="/login" element={<LoginPage />}></Route>
          <Route path="/" element={<ProfilePage />}></Route>
        </Routes>
      </div>
    </div>
  );
}

export default App;
